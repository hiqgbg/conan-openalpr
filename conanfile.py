from conans import ConanFile, CMake, tools


class OpenalprConan(ConanFile):
    name = "openalpr"
    version = "2.3.0"
    license = "AGPL-3.0"
    author = "Anders Arnholm"
    url = "https://bitbucket.org/hiqgbg/conan-openalpr/"
    description = "Automatic License Plate Recognition library http://www.openalpr.com"
    topics = ("openalpr", "opencv", "conan", "computer-vision", "image-processing", "deep-learning")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"

    def source(self):
        self.run("git clone https://github.com/openalpr/openalpr.git")
        self.run("cd openalpr && git fetch --tags && git checkout v2.3.0")
        # This small hack might be useful to guarantee proper /MT /MD linkage
        # in MSVC if the packaged project doesn't have variables to set it
        # properly
        tools.replace_in_file("openalpr/src/CMakeLists.txt", "project(src)",
                              '''project(src)
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()''')

    def requirements(self):
        self.requires.add('opencv/3.4.5@conan/stable')
        self.requires.add('tesseract/4.0.0@bincrafters/stable')
        self.requires.add('leptonica/1.76.0@bincrafters/stable')

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="openalpr/src")
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include/openalpr", src="openalpr/src/openalpr")
        self.copy("*hello.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["openalpr"]

